<?php

namespace atiline\KartikGridViewColumnsVisibility\migrations;


use yii\db\Migration;

/**
 * Class m_atiline_kartik_grid_view_columns_visibility
 * @package atiline\KartikGridViewColumnsVisibility\migrations
 */
class m_atiline_kartik_grid_view_columns_visibility extends Migration
{
    /**
     * @return bool|void
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        $this->execute(file_get_contents(__DIR__ . '/m_atiline_kartik_grid_view_columns_visibility_UP.sql'));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->execute(file_get_contents(__DIR__ . '/m_atiline_kartik_grid_view_columns_visibility_DOWN.sql'));
    }

}
