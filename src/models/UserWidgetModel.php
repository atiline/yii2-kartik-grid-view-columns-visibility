<?php

namespace atiline\KartikGridViewColumnsVisibility\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Json;

/**
 * This is the model class for table "additional_field_table".
 *
 * @property int $user_id
 * @property string $widget_id
// * @property string $data
 * @property int $created_at
 * @property int $updated_at
 *
 * @property mixed $data_arr
 */
class UserWidgetModel extends \yii\db\ActiveRecord
{

    public $data_arr;


    /**
     * @param $widget_id
     * @param $column_id
     */
    public static function onColumnRemove($widget_id, $column_id) {

        /** @var self[] $userDatas */
        $userDatas = self::find()->where(['widget_id' => $widget_id])->all();

        foreach ($userDatas as $userData) {

            $data = $userData->data_arr;

            $_data = [];
            foreach ($data as $value) {
                if($value != $column_id) {
                    $_data[] = $value;
                }
            }

            $userData->data_arr = $_data;
            $userData->save();
        }
    }

    /**
     * @param int $user_id
     * @param string $widget_id
     * @return array|null
     */
    public static function getDataForUser($user_id, $widget_id) {

        $userWidget = self::findOne(['user_id' => $user_id, 'widget_id' => $widget_id]);

        if($userWidget) {
            return $userWidget->data_arr;
        }

        return [];
    }

    /**
     * @param int $user_id
     * @param string $widget_id
     * @return bool
     */
    public static function setDataForUser($user_id, $widget_id, $data) {

        $userWidget = self::findOne(['user_id' => $user_id, 'widget_id' => $widget_id]);

        if(!$userWidget) {
            $userWidget = new self();
            $userWidget->user_id = $user_id;
            $userWidget->widget_id = $widget_id;
        }

        $userWidget->data_arr = $data;

        return $userWidget->save();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'atiline_kartik_grid_view_columns_visibility_data';
    }

    public static $dbConn = 'db';

    /**
     * @return mixed|\yii\db\Connection
     */
    public static function getDb() {
        $component = static::$dbConn;
        return Yii::$app->$component;
    }



    public function beforeSave($insert)
    {
        $this->data = $this->data_arr ? Json::encode(array_unique($this->data_arr)) : '';
        return parent::beforeSave($insert);
    }


    public function afterFind()
    {
        parent::afterFind();

        $this->data_arr = $this->data ? array_unique(Json::decode($this->data)) : [];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'widget_id'], 'required'],
            [['user_id'], 'integer'],
            [['data', 'data_arr'], 'safe'],
            [['widget_id'], 'string', 'max' => 512],
            [['user_id', 'widget_id'], 'unique', 'targetAttribute' => ['user_id', 'widget_id']],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }
}
