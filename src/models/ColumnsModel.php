<?php

namespace atiline\KartikGridViewColumnsVisibility\models;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Json;

/**
 * This is the model class for table "atiline_kartik_grid_view_columns_visibility_columns_data".
 *
 * @property string $widget_id
 * @property string $name
 * @property string $id
 * @property int $sort_order
 * @property int $created_at
 * @property int $updated_at
 *
 *
 */
class ColumnsModel extends \yii\db\ActiveRecord
{


    /**
     * @param $widget_id
     * @return self[]
     */
    public static function getColumnsForWidget($widget_id) {
        return self::find()->where(['widget_id' => $widget_id])->orderBy(['sort_order' => SORT_ASC])->indexBy('id')->all();
    }

    /**
     * @param string $widget_id
     * @param array $columns
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function checkColumns($widget_id, array $columns) {

        $sort_order = 0;

        foreach ($columns as $id => $column) {
            $sort_order += self::SORT_ORDER_STEP;
            $columns[$id]['sort_order'] = $sort_order;
        }

        /** @var self[] $currents */
        $currents = self::find()->where(['widget_id' => $widget_id])->orderBy(['sort_order' => SORT_ASC])->indexBy('id')->all();


        $order_changed = false;

        foreach ($columns as $id => $column) {

            if(!isset($currents[$id])) {
                $currents[$id] = new self();
                $currents[$id]->name = $column['name'];
                $currents[$id]->widget_id = $widget_id;
                $currents[$id]->id = $id;
                $currents[$id]->sort_order = $column['sort_order'];
                $currents[$id]->save();

                $order_changed = true;
            }

            if($currents[$id]->sort_order != $column['sort_order']) {
                $currents[$id]->sort_order = $column['sort_order'];
                $currents[$id]->save();
                $order_changed = true;
            }
        }

        foreach ($currents as $id_currents => $current) {
            if(!isset($columns[$id_currents])) {
                $currents[$id_currents]->delete();
                $order_changed = true;
                UserWidgetModel::onColumnRemove($widget_id, $id_currents);
            }
        }

        if($order_changed) {
            self::reorderColumns($widget_id);
        }

        return true;
    }


    private static function reorderColumns($widget_id) {
        /** @var self[] $currents */
        $currents = self::find()->where(['widget_id' => $widget_id])->orderBy(['sort_order' => SORT_ASC])->indexBy('id')->all();

        $sort_order = 0;

        foreach ($currents as $current) {
            $sort_order += self::SORT_ORDER_STEP;
            $current->sort_order = $sort_order;
            $current->save();
        }
    }


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'atiline_kartik_grid_view_columns_visibility_columns_data';
    }

    public static $dbConn = 'db';

    /**
     * @return mixed|\yii\db\Connection
     */
    public static function getDb() {
        $component = static::$dbConn;
        return Yii::$app->$component;
    }


    const SORT_ORDER_STEP = 100;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['widget_id', 'name', 'id', 'sort_order'], 'required'],
            [['sort_order'], 'integer'],
            [['widget_id'], 'string', 'max' => 512],
            [['name'], 'string', 'max' => 2048],
            [['id'], 'string', 'max' => 50],
            [['widget_id', 'id'], 'unique', 'targetAttribute' => ['widget_id', 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }
}