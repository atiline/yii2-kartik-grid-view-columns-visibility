<?php


use kartik\sortinput\SortableInput;
use yii\bootstrap4\Html;


/* @var $this \yii\web\View */
/* @var $formModel \atiline\KartikGridViewColumnsVisibility\Module\models\UserWidgetForm */


$user_items = [];

foreach ($formModel->user_columns_arr as $id => $name) {
    $user_items[$id] = ['content' => $name];
}

$items = [];

foreach ($formModel->columns_arr as $id => $name) {
    $items[$id] = ['content' => $name];
}

$css = <<<CSS

.sortable.list {
min-height: calc(1rem + 34px);
}
CSS;
$this->registerCss($css);
?>

<?php $form = \yii\bootstrap4\ActiveForm::begin() ?>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col">
                    <?= $form->field($formModel, 'columnsString')->widget(SortableInput::class, [
                        'items' => $items,
                        'sortableOptions' => [
                            'connected' => true,
                        ],
                        'options' => ['class' => 'form-control', 'readonly' => true]
                    ]) ?>
                </div>
                <div class="col">
                    <?= $form->field($formModel, 'userColumnsString')->widget(SortableInput::class, [
                        'items' => $user_items,
                        'sortableOptions' => [
                            'connected' => true,
                        ],
                        'options' => ['class' => 'form-control', 'readonly' => true]
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
<?php if (!Yii::$app->request->isAjax) { ?>
    <br>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
<?php } ?>
<?php \yii\bootstrap4\ActiveForm::end() ?>