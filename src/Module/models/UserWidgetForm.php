<?php

namespace atiline\KartikGridViewColumnsVisibility\Module\models;
use atiline\KartikGridViewColumnsVisibility\models\ColumnsModel;
use atiline\KartikGridViewColumnsVisibility\models\UserWidgetModel;
use Behat\Gherkin\Parser;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class UserWidgetForm
 * @package atiline\KartikGridViewColumnsVisibility\Module\models
 *
 * @property array $columns_arr
 * @property array $user_columns_arr
 * @property array $all_columns
 *
 * @property string $widget_id
 * @property int $user_id
 *
 *
 * @property string $userColumnsString
 * @property string $columnsString
 *
 */
class UserWidgetForm extends Model
{

    public $widget_id;
    public $user_id;

    public $columns_arr = [];
    public $user_columns_arr = [];

    public $all_columns = [];


    public function getColumnsString() {
        return implode(',', array_keys($this->columns_arr));
    }

    public function setColumnsString($value) {

        $ids = explode(',', $value);

        $this->columns_arr = [];

        foreach ($ids as $id) {
            $_val = ArrayHelper::getValue($this->all_columns, $id, null);

            if($_val) {
                $this->columns_arr[$id] = $_val;
            }
        }
    }

    public function getUserColumnsString() {
        return implode(',', array_keys($this->user_columns_arr));
    }

    public function setUserColumnsString($value) {

        $ids = explode(',', $value);

        $this->user_columns_arr = [];

        foreach ($ids as $id) {
            $_val = ArrayHelper::getValue($this->all_columns, $id, null);

            if($_val) {
                $this->user_columns_arr[$id] = $_val;
            }
        }
    }


    public function init()
    {
        parent::init();

        if(!$this->widget_id) {
            throw new InvalidConfigException('widget_id must be set');
        }

        if(!$this->user_id) {
            throw new InvalidConfigException('user_id must be set');
        }

        foreach (ColumnsModel::getColumnsForWidget($this->widget_id) as $column) {
            $this->all_columns[$column->id] = $column->name;
        }

        $user_columns_ids = UserWidgetModel::getDataForUser($this->user_id, $this->widget_id);

        foreach ($user_columns_ids as $user_columns_id) {
            $_val = ArrayHelper::getValue($this->all_columns, $user_columns_id, null);

            if($_val) {
                $this->user_columns_arr[$user_columns_id] = $_val;
            }
        }

        foreach ($this->all_columns as $id => $column) {

            if(!in_array($id, $user_columns_ids)) {
                $this->columns_arr[$id] = $column;
            }
        }

        if(count($this->user_columns_arr) == 0) {
            $_arr = $this->user_columns_arr;
            $this->user_columns_arr = $this->columns_arr;
            $this->columns_arr = $_arr;
        }
    }


    public function save() {


        return UserWidgetModel::setDataForUser($this->user_id, $this->widget_id, array_keys($this->user_columns_arr));

    }


    public function attributeLabels()
    {
        return [
            'userColumnsString' => 'Показывать',
            'columnsString' => 'Не показывать'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userColumnsString', 'columnsString'], 'string'],
        ];
    }

}