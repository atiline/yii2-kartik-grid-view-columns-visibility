<?php

namespace atiline\KartikGridViewColumnsVisibility\Module;

/**
 * KartikGridViewColumnsVisibility module definition class
 */
class KartikGridViewColumnsVisibility extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'atiline\KartikGridViewColumnsVisibility\Module\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}