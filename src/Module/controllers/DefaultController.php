<?php

namespace atiline\KartikGridViewColumnsVisibility\Module\controllers;

use atiline\KartikGridViewColumnsVisibility\Module\models\UserWidgetForm;
use Yii;
use yii\bootstrap4\Html;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;

/**
 * Default controller for the `KartikGridViewColumnsVisibility` module
 */
class DefaultController extends Controller
{

    /**
     * @param $widget_id
     * @return string
     */
    public function actionIndex($widget_id, $pjax_to_reload = null)
    {

        $request = \Yii::$app->request;

        $formModel = new UserWidgetForm(['user_id' => \Yii::$app->user->id, 'widget_id' => $widget_id]);

        if ($request->isAjax) {

            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($formModel->load(\Yii::$app->request->post()) && $formModel->save()) {


                $res = [];
                $res['forceClose'] = true;
                if($pjax_to_reload) {
                    $res['forceReload'] = '#' . $pjax_to_reload;
                }
                return $res;
            }

            return [
                'title' => "Список колонок",
                'content' => $this->renderAjax('index', [
                    'formModel' => $formModel
                ]),
                'footer' => Html::button('Закрыть', ['class' => 'btn btn-default ml-0 mr-auto', 'data-dismiss' => "modal"]) .
                    Html::button('Применить', ['class' => 'btn btn-primary', 'type' => "submit"])

            ];

        } else {

            if ($formModel->load(\Yii::$app->request->post()) && $formModel->save()) {

            }

            return $this->render('index', [
                'formModel' => $formModel
            ]);
        }


    }
}
