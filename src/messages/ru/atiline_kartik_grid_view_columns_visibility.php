<?php

return [
    'Serial column' => 'Порядковый номер',
    'Checkbox column' => 'Чекбокс',
    'Action column' => 'Действия',
    'Data column' => 'Данные',
    'Formula column' => 'Формула',
    'Boolean column' => 'Булевая',
    'Enum column' => 'Перечисление',
    'Expand row column' => 'Раскрывающааяся',
    'Radio column' => 'Радио',
    'Undefined column' => 'Неизвестная',
];
