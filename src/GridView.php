<?php

namespace atiline\KartikGridViewColumnsVisibility;


use atiline\KartikGridViewColumnsVisibility\models\ColumnsModel;
use atiline\KartikGridViewColumnsVisibility\models\UserWidgetModel;
use kartik\base\TranslationTrait;
use kartik\grid\ActionColumn;
use kartik\grid\BooleanColumn;
use kartik\grid\CheckboxColumn;
use kartik\grid\EnumColumn;
use kartik\grid\ExpandRowColumn;
use kartik\grid\FormulaColumn;
use kartik\grid\RadioColumn;
use kartik\grid\SerialColumn;
use ReflectionClass;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQueryInterface;
use yii\grid\Column;
use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Json;

/**
 * Class GridView
 * @package atiline\KartikGridViewColumnsVisibility
 *
 * @property string $columnsVisibilityId
 * @property array $columns_ids
 *
 * @property Column[] $_columns
 */
class GridView extends \kartik\grid\GridView {


    use TranslationTrait;

    public $columnsVisibilityId = null;

    const I18N_CAT_NAME = 'atiline_kartik_grid_view_columns_visibility';

    private $columns_ids = [];

    private $_columns = [];

    public function init()
    {

        $this->initI18N('', self::I18N_CAT_NAME);

        if($this->columnsVisibilityId === null) {
            $this->columnsVisibilityId = $this->id . '_columns_visibility';
        }

        return parent::init();
    }

    /**
     * Creates column objects and initializes them.
     */
    protected function initColumns()
    {
        if (empty($this->columns)) {
            $this->guessColumns();
        }

        $arr = [];

        foreach ($this->columns as $id => $column) {

            if(in_array($id, $this->columns_ids)) {
                throw new InvalidArgumentException('Id ' . $id . ' is not unique!!');
            }

            $this->columns_ids[] = $id;

            $name = null;

            $name = ArrayHelper::getValue($column, 'label', null);

            if (is_string($column)) {
                $column = $this->createDataColumn($column);
            } else {
                $column = Yii::createObject(array_merge([
                    'class' => $this->dataColumnClass ?: DataColumn::className(),
                    'grid' => $this,
                ], $column));
            }

            $this->_columns[$id] = $column;


            if($name === null && $column instanceof \kartik\grid\DataColumn) {
                $name = $this->getDataColumnLabel($column);
            }

            if($name === null) {
                if($column instanceof SerialColumn) {
                    $name = Yii::t(self::I18N_CAT_NAME, 'Serial column');
                }
                if($column instanceof CheckboxColumn) {
                    $name = Yii::t(self::I18N_CAT_NAME, 'Checkbox column');
                }
                if($column instanceof ActionColumn) {
                    $name = Yii::t(self::I18N_CAT_NAME, 'Action column');
                }
                if($column instanceof DataColumn) {
                    $name = Yii::t(self::I18N_CAT_NAME, 'Data column');
                }
                if($column instanceof FormulaColumn) {
                    $name = Yii::t(self::I18N_CAT_NAME, 'Formula column');
                }
                if($column instanceof BooleanColumn) {
                    $name = Yii::t(self::I18N_CAT_NAME, 'Boolean column');
                }
                if($column instanceof EnumColumn) {
                    $name = Yii::t(self::I18N_CAT_NAME, 'Enum column');
                }
                if($column instanceof ExpandRowColumn) {
                    $name = Yii::t(self::I18N_CAT_NAME, 'Expand row column');
                }
                if($column instanceof RadioColumn) {
                    $name = Yii::t(self::I18N_CAT_NAME, 'Radio column');
                }
            }

            if($name === null) {
                $name = Yii::t(self::I18N_CAT_NAME, 'Undefined column');
            }

            $arr[$id] = [
                'name' => $name
            ];

        }

        ColumnsModel::checkColumns($this->columnsVisibilityId, $arr);

        $userColumns = UserWidgetModel::getDataForUser(Yii::$app->user->id, $this->columnsVisibilityId);

        $this->columns = [];

        if(count($userColumns) > 0) {
            $newColumns = [];
            foreach ($userColumns as $id) {

                $column_to_add = $this->_columns[$id];

                if($column_to_add && $column_to_add->visible) {
                    $this->columns[] = $column_to_add;
                }
            }
        } else {
            foreach ($this->_columns as $column) {
                if($column->visible) {
                    $this->columns[] = $column;
                }
            }
        }
    }


    private function getDataColumnLabel(DataColumn $column) {
        $provider = $this->dataProvider;

        if ($column->label === null) {
            if ($provider instanceof ActiveDataProvider && $provider->query instanceof ActiveQueryInterface) {
                /* @var $modelClass Model */
                $modelClass = $provider->query->modelClass;
                $model = $modelClass::instance();
                $label = $model->getAttributeLabel($column->attribute);
            } elseif ($provider instanceof ArrayDataProvider && $provider->modelClass !== null) {
                /* @var $modelClass Model */
                $modelClass = $provider->modelClass;
                $model = $modelClass::instance();
                $label = $model->getAttributeLabel($column->attribute);
            } elseif ($this->filterModel !== null && $this->filterModel instanceof Model) {
                $label = $this->filterModel->getAttributeLabel($column->attribute);
            } else {
                $models = $provider->getModels();
                if (($model = reset($models)) instanceof Model) {
                    /* @var $model Model */
                    $label = $model->getAttributeLabel($column->attribute);
                } else {
                    $label = Inflector::camel2words($column->attribute);
                }
            }
        } else {
            $label = $column->label;
        }

        return $label;
    }

}